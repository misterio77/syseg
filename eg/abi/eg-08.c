/* Calling convention: global-variable method. */


int arg1;			/* First argument.  */
int arg2;			/* Second argument. */

int foo ()
{
  return arg1 - arg2;
}

int main()
{
  arg1 = 5;
  arg2 = 2;

  return foo();
}
