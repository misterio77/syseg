/* Calling convention: register method. */

int __attribute__((fastcall)) foo (int arg1, int arg2)
{
  return arg1 - arg2;
}


int main()
{
  return foo (5,2);
}
