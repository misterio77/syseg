
int foo()
{
  int register var __asm__("ebx");
  var += 1;
  return var;
}

int main ()
{
  foo();
  return 0;
}
