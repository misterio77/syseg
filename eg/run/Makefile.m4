
dnl  Makefile.m4 - Makefile template.
dnl    
dnl  Copyright (c) 2021 - Monaco F. J. <monaco@usp.br>
dnl
dnl  This file is part of SYSeg. 
dnl
dnl  SYSeg is free software: you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation, either version 3 of the License, or
dnl  (at your option) any later version.
dnl
dnl  SYSeg is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with .  If not, see <http://www.gnu.org/licenses/>.
dnl
dnl  ------------------------------------------------------------------
dnl
dnl  Note: this is a source file used to produce either a documentation item,
dnl        script or another source file by m4 macro processor. If you've come
dnl	   across a file named, say foo.m4, while looking for one of the
dnl	   aforementioned items, changes are you've been looking for file foo,
dnl	   instead. If you can't find foo, perhaps it is because you've missed
dnl 	   the build steps described in the file README, found in the top 
dnl	   source directory of this project.
dnl        
dnl
include(docm4.m4)dnl
DOCM4_HASH_HEAD_NOTICE([Makefile],[Makefile script.])

bin = eg-01 

WARN = -Wall -Wno-unused-result -Wno-parentheses

CPP_FLAGS=  $(WARN) $(CPPFLAGS)
C_FLAGS= -Og -m32 -fcf-protection=none  $(CFLAGS)
LD_FLAGS= -m32 $(LDFLAGS)

#
# eg-01
#

eg-01.o : eg-01.c
	gcc -c $(CPP_FLAGS) $(C_FLAGS) $< -o $@

eg-01 : eg-01.o
	gcc $(LD_FLAGS) $< -o $@

#
# eg-02
#

eg-02-static.o eg-02a-static.o: %-static.o : %.c
	gcc -c -fno-pic -fno-pie $(CPP_FLAGS) $(C_FLAGS) $< -o $@

libeg-02.a : eg-02a-static.o
	ar -rcs $@ $^

eg-02-static : eg-02-static.o libeg-02.a
	gcc -no-pie $(LD_FLAGS) -L.  eg-02-static.o -leg-02 -o $@

#

eg-02-rel.o eg-02a-rel.o: %-rel.o : %.c
	gcc -c -fno-pic -fno-pie $(CPP_FLAGS) $(C_FLAGS) $< -o $@

libeg-02-rel.so : eg-02a-rel.o
	gcc --shared -fno-pic $(LD_FLAGS) $^ -o $@

eg-02-rel : eg-02-rel.o libeg-02-rel.so
	gcc $(LD_FLAGS)  -L. eg-02-rel.o  -leg-02-rel -fno-pic -fno-pie -o eg-02-rel

#

eg-02-pic.o eg-02a-pic.o: %-pic.o : %.c
	gcc -c -fpic -fpie $(CPP_FLAGS) $(C_FLAGS) $< -o $@

libeg-02-pic.so : eg-02a-pic.o
	gcc --shared -fpic $(LD_FLAGS) $^ -o $@

eg-02-pic : eg-02-pic.o libeg-02-pic.so
	gcc $(LD_FLAGS)  -L. eg-02-pic.o  -leg-02-pic -fpic -fpie -o eg-02-pic



##
## Housekeeping
##

.PHONY: clean
clean:
	rm -f $(bin)
	rm -f *-rel *-pic *-static *.a *.so *.d *.o


UPDATE_MAKEFILE

DOCM4_MAKE_BINTOOLS

# Local rules

# ps :
# 	term=$$(ps -p $$$$ -o tty=); while true; do ps axf | grep "$$term" ; sleep 1; clear; done 


ps :
	while true; do ps axf | grep "$(TERM)" ; sleep 1; clear; done 


