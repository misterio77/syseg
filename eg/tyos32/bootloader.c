/* main.c - Main program of MbrCmd
 
   Copyright (c) 2021, Monaco F. J. <monaco@usp.br>

   This file is part of SYSeg.

   SYSeg is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <utils.h>


void __attribute__((naked)) load_stage2();

void init32();   /* This function is the entry point of stage2. */

int main()
{
  clear();
  
  echo (" Stage 1: loading second stage..." NL NL);

  load_stage2();

  init32();     
  
  return 0;
}

extern char drive;

/* Load stage2. */

void __attribute__((naked)) load_stage2()
{
  __asm__  
    (

     /* Save all registers. */

     "  pusha                   ;" /* Save all registers (note 1). */

     /* Compute the size of the second stage. */

     "  xor %dx, %dx;"
     "  mov $_STAGE2_SIZE, %ax;"  /* Stage2 size determined by the linker. */
     "  mov $512, %cx;"
     "  div %cx;"
     "  add $1, %ax;"
     "  mov %ax, size2;"	 

     
     /* Reset floppy just for the case. */
     
     "reset:                      ;"
     "    mov $0x0, %ah           ;" /* Teset disk.       */
     "    mov drive, %dl          ;" /* The boot drive    */
     "    int $0x13               ;" /* Call BIOS.        */
     
     "    jnc load                ;" /* On error, abort.  */     
     "    mov $err2, %cx          ;"
     "    call fatal              ;" 
     
     /* Load stage 2.   */

     " load:                     ;"
     
     "   mov drive, %dl          ;" /* The boot drive.           */
     "   mov $0x2, %ah           ;" /* Means read sector.        */
     "   mov size2, %al          ;" /* Number of sectors to read */
     "   mov $0x0, %ch           ;" /* Cylinder (starts at 0)    */
     "   mov $0x2, %cl           ;" /* Sector   (starts at 1)    */
     "   mov $0x0, %dh           ;" /* Head     (starts at 0)    */
     "   mov $_STAGE2_ADDR , %bx ;" /* Where to load data.       */
     "   int $0x13               ;" /* Call BIOS.                */

     "   mov $err1, %cx          ;"
     "   jc fatal                ;" /* On error, abort.          */


     /* Restore all registers. */

     "  popa                     ;"   
     "   ret                     ;"
     );
}

const char err1[] = " Error: device reset failure." NL;
const char err2[] = " Error: device read failure." NL;

/* 
   Notes.

   (1) When we program in C, the compiler has total control of which registers
       it uses. When the program calls a function, and the function changes
       the registers, the compiler may decide to save the registers when
       entering the function and restoring them when returning from the 
       function.

       When we add inline asm, we're messing with reigsters in a way that 
       the compiler is not aware of. Because of this, it's a good ideia to save
       and restore the registers manually. We could do it only for the
       registers we change in the inline asm; or we can simply save and restore
       all general purpose registers.

 */
