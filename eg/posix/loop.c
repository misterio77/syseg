#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main (int argc, char **argv)
{
  int count = atoi(argv[1]);

  while (count)
    {
      printf ("Loop: %d\n", count--);
      sleep(1);
    }
  
  return EXIT_SUCCESS;
}
