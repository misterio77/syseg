#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int main()
{
  pid_t pid;
  
  /* Function fork() returns to the
      - parent: child's pid;
      - child : 0 (zero)              */
  
  pid = fork();
  
  if (pid>0)			/* Parent will execute this. */
    {
      printf ("Parent (pid %d): I'm your father.\n", getpid());
    }
  else				/* Child will execute this.  */
    {
      printf ("Child  (pid %d): Nooooo...\n", getpid());
    }

  
  return EXIT_SUCCESS;
}
