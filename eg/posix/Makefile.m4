include(docm4.m4)dnl
DOCM4_HASH_HEAD_NOTICE([Makefile],[Make script])

init: all 

UPDATE_MAKEFILE

##
## Relevant rules
##

CPP_FLAGS = $(CPPFLAGS)
C_FLAGS   = -Wall -Wno-parentheses $(CFLAGS)
LD_FLAGS  = $(LDFLAGS)


bin = getpid fork fork-01 fork-02 exec exec-01 wait loop open redir shell-loop shell-redir shell-pipe shell-job pipe pipeline kill sigaction sigaction-handler ctermid pgid tcpgrp

all: $(bin)

$(bin) : % : %.c
	gcc $(CPP_FLAGS) $(C_FLAGS) $(LD_FLAGS) $< -o $@

exec-01 : loop
kill : CPPFLAGS=-D_GNU_SOURCE
sigaction : CPPFLAGS=-D_GNU_SOURCE
sigaction-handler: CPPFLAGS=-D_GNU_SOURCE



.PHONY: clean

clean :
	rm -f *.o $(bin)
	rm -f *~

dnl
dnl Uncomment to include bintools
dnl
dnl
dnl DOCM4_MAKE_BINTOOLS

