include(docm4.m4)

 BARE-METAL HELLO WORLD
 ==============================

DOCM4_DIR_NOTICE

 Overview
 ------------------------------

 This directory contains a series of code examples that illustrate the
 step-by-step implementation of a x86 bare-metal "Hello World" program,
 which can be booted and executed on a real piece of hardware.

 The sequence starts as simple as a program written directly in machine
 code, which is then rewritten in pure assembly, and finally translated
 into C language.


DOCM4_INSTRUCTIONS

 Contents
 ------------------------------


 Take a look at the following examples, preferably in the suggested order.
 

 * eg-00.hex	    Bare-metal hardcore Hello World in machine code.
 
   		    This is a tough old-school version of the classical
		    Hello World program. It writes the string directly to the
		    BIOS video memory, character by character, one at a time.
		    A Hello World as you may have never made before, with
		    neither an OS, nor a programming language, not even
		    assembly; just bare machine code.

		    The source code is an ASCII file containing the opcodes of
		    the program in hexadecimal representation.

		    Build with it with a simple hex-to-binary converter:

		       make eg-00.bin

		    Test with the emulator

 		       make eg-00.bin/run

		    Transfer eg-00.bin to a USB sick 

		       make eg-00.bin/stick <your-device e.g. /dev/sdb>

		    and boot it in the real PC hardware. If it does not work,
		    please refer to "Note on booting the real hardware" ahead
		    in this document.

		    Note: instead of using eg-00.hex, you may also create your
		    own hexfile, say hello.hex, by writing the first 49 bytes of
		    eg-00.hex, one per line, and then

		    	for ((i=50; i<=512; i++)); do echo 0; done >> hello.hex

 * eg-00-r1.hex	    The same as eg-00.hex, but handling VBR-oriented BIOSes

   		    This is example is provided to handle some idiosyncratic
		    BIOSes that may either not boot or else boot a corrupted
		    initialization program.

   		    See eg-01-r1.asm for details.

 * eg-00-r2.hex	    The same as eg-00-r1.hex, but with a valid BPB.

   		    There has been no reports about eg-00-r1.hex not working,
		    but just for the case, this is an alternative version
		    with a valid BPB.

   		    See eg-01-r2.asm for details.


 * eg-01.asm        The same as eg-00.hex, but now in assembly code.

   		    This program implements literally the same algorithm as
		    eg-01.hex, but written in assembly code, using Intel
		    syntax. This is the assembly understood by NASM assembler,
		    used here to build the binary.

		    Compare eg-01.bin and eg-02-beta.1.bin, by disassembling
		    them with

		      make eg-00.bin/16i
		      make eg-01.bin/16i

                    or, alternatively, with the graphical tool

		      make i16 eg-00.bin eg-01.bin

		    and see that the resulting object codes match.

		    We are forcing the disassembler to interpret the object
		    as code for a i8086 (16-bit) CPU, using intel asm syntax.

 * eg-01-r1.asm	    The same as eg-01.asm, but handling VBR-oriented BIOSes.

   		    Try this if your eg-01.bin works as expected with the
		    emulator but not with the physical hardware.

   		    Some idiosyncratic BIOSes assume that the boot media
		    contains a Volume Boot Record in the first 512-bytes,
		    as expected in a FAT-formatted disk. Such BIOSes may
		    either refuse to boot if a VBR signature is not found,
		    or else even boot but then corrupt the loaded program.

		    The the notes in eg-01-r1.asm for further explanation.

 * eg-01-r2.asm	    The same as eg-01-r1.asm, but with a valid BPB

   		    This example implements a BPB with valid values, should some
		    BIOS checks for it.

 * eg-02-alpha.asm  A variation of eg-01.asm, using a loop.

   		    This code does not work. Can you spot the problem?

 * eg-02-beta.asm   Same as eg-02-alpha.asm, but fixed.

  		    Now that we are dealing with memory references, we must
		    beware of the RAM load address. In this example, we handle
		    this by manually adding the offset in the code where
		    needed.

 * eg-02-beta-r1    Same as eg-02-beta.asm, but using a macro.

   		    Alternative way to manually add the load-address offset.
		    
 * eg-02.asm	    Same as eg-02-beta.2.asm, but using the 'org' directive.

   		    The more elegant the systematic way to handle
		    load-address offset.

   		    The directive 'org' causes all labels to be offset
		    to match the position where BIOS loads the program.

		    
		    Compare eg-02-beta.bin and eg-02.bin with

		      make eg-02-beta.bin/i16 
		      make eg-02.bin/i16

		    or

		      make i16 eg-02-beta.bin eg-02.bin
  		    
		    and see that the resulting object codes match.

		    Now it's opportune to observe that NASM is performing
		    both the assembling (object code generation) and
		    linking (build-time address relocation) steps.



* eg-03-beta.S      Like eg-02.asm, but translated into AT&T assembly syntax.

  		    One motivation to convert the assembly source code from
		    Intel to AT&T dialect is because we intend to rewrite
		    the running example in C language.  To that end, we will
		    use the powerful and flexible GNU compiler collection.
		    The thing is, GCC's toolchain speaks AT&T asm and does not
		    understand NASM's intel asm.

		    See note (2) for further detailing.

   		    The translation was made as literal as possible so as to
		    facilitate the comparison with eg-04.asm.

		    A noteworthy observation is that the build procedure of
		    this example involves two steps: assembling and linking.
		    The former translates assembly to object (machine) code,
		    and the latter remaps offsets to match the load address.
		    Those are the normal duties of the assembler and the linker.
		    With NASM, which we had been using previously, those
		    two functions are performed by the same program. In the
		    GNU development toolchain, differently, those duties are
		    performed by two distinct programs: the assembler (GAS,
		    whose executable program name is 'as') converts assembly
		    to object code (machine code), and then the linker (ld)
		    is used to remap the addresses offset to match the RAM
		    load address. GAS does not have an equivalent for the
		    .org directive that we used with NASM for this purpose;
		    we inform the linker directly via a command line option.

		    Moreover, whilst we had instructed NASM to produce a flat
		    binary, the version of GAS installed in our x86 / x86_64
		    platform outputs binaries in ELF format --- a structured
		    executable format that has much more than the executable
		    code section (e.g. header, relocation information and
		    other metadata). It is the liker who is responsible to
		    strip all extra sections and output a flat binary.
		    We tell ld to do that using another command-line option.

		    Finally, ld let us specify which will be the entry point
		    of the executable. This would be important for structured
		    file formats such as ELF (Linux) or PE (windows). For
		    us, it's the first byte. However, since ld dos expect it,
		    we pass this information in yet another command-line option.


 * eg-03.S	    Same as before, but with standard entry-point name.

   		    This example is to illustrate that the default symbol name
		    for the entry point is _start. If we stick to it, we do
		    not need to pass it as an option for to the linker. 

		    Alternative:

		    Issuing the build rule with the command-line variable
		    ALT=1 selects an alternative recipe using a single GCC 
		    invocation (GCC then invokes the assembler and the linker
		    with appropriate arguments).

		    Disassemble (AT&T syntax, i8080 cpu) with 

		      make eg-02.bin/a16 
		      make eg-03.bin/a16

		      or

  		      make a16 eg-02.bin eg-03.bin 
		      
		    and see that they all binaries match, i.e. the output of
		    both assemblers (NASM and GAS) are the same.


 * eg-04-alpha.c   Like eg-03.S but rewritten in C and inline assembly (buggy)

   		    We use basic inline assembly. GCC should copy the asm code
                    as is to the output asm.  The two function-like macros
		    are expanded by the preprocessor in build time.

		    Caveats:

                    We declared the function with attribute 'naked' to prevent
                    GCC from generating extra asm code which is not relevant
                    here and may be omitted for readability. See comments
                    in the source file.

		    Notice that the string is placed before the executable code.
		    Since the code will be executed from the very first byte,
		    the program will not work. This is a problem.

		    Notice also that even if we move the string  to some place
		    after the executable code, the string is still allocated in
		    the read-only data section. This is also a problem because
		    the labels are offsets relative to the start of the section
		    where they are defined, and this the string label won't be
		    correctly accessible from within the .text section.

 * eg-04-beta.c	    Like eg-04-alpha.c, but the string is moved to the end.

   		    Observe the ad hoc tactic (aka plain hack) we used to work
		    around the compiler's default policy to allocate the
		    string in the .rodata section.


 * eg-04-beta.1.c    Like eg-04-beta, but with asm code in a header file.

  		    Now the source looks a bit more like a C code.
  		    

 * eg-04.c	    Like eg-04-beta-1.c, but using a linker script.

   		    The provided eg-04.ld script handles several issues:

   		    - merge .rodata into .text section

		    as well as

   		    - add the the boot signature        
   		    - convert from elf to binary         
   		    - set load address                  
   		    - set entry point


		    The first feature frees our code from the hack on .rodata
		    problem, and the latter four simplify ld command-line options.


		    Compare eg-03.bin and eg-04.bin with

		    	make a16 eg-03.bin eg-04.bin

                    Both object codes are almost a perfect match.

		    Bearing in mind that, since there is possibly more than
		    one way to implement the behavior specified by the C
		    source code into assembly form, it is not guaranteed
		    that both the handwritten eg-03.bin and the GCC-produced
		    eg-04.bin will match.
		    
		    Indeed if we inspect the disassembled objects, we may see
		    that GCC added a few extra instructions after the code.
		    Those are no-operation (NOP) and undefined (UD2)
		    instructions that are inserted by the compiler as a
		    security measure to assert that code is not executed
		    past the end of the legit code.  The appended
		    instructions however cause the string to be moved some
		    positions away from its original place in eg-03.S.
		    This is reflected in some other parts of the code.
		    

 * eg-05.c	    A rewrite of eg-04.c replacing macros with actual functions.

   		    This code introduces a new form of addressing memory in
		    AT&T syntax. See note (3) for the rationales.

		    Moreover, remember that when a function calls another
		    function, the caller needs to save the address to where
		    callee must return to after its execution completes.
		    The call instruction pushes the return address onto
		    the stack. The top of the stack is pointed to by register
		    SP. Up to now we have tacitly assumed that, when our 
		    program starts, the value left in SP by the BIOS is a good
		    one. The rationale for this guess is that the BIOS itself
		    uses the stack and it must have already initialized SP.
		    That is a hopeful bet, though. The right thing to do is
		    to explicitly initialize the stack.

		    To initialize the stack, we use a function-like macro
		    that expands into an inline asm statement. Just remember
		    that we can't implement it as a function because, well,
		    we would need an initialized tack to call functions.

		    This macro must be placed right at the beginning of
		    the function _start() before we call any other function
		    or interrupt calls.

		    Note: we elected __start() as the entry point of our
		    program. Technically, the function is not "called"
		    by any other function and, therefore, there's no
		    problem if SP is only initialized by __start() itself.
		    Also, our function halt() never returns; it just halts
		    the computer (this is not how conventional programs
		    end; we're simplifying things for now).

 * eg-06.c 	    Using a runtime initializer.

   		    To make our program look like a bit more like a regular
		    C program, we moved _start() to another compilation
		    unit named rt0. In conventional C runtime  such as in
		    Linux and Windows, _start() (usually implemented by
		    the object file greet0.o) calls main(), working as both
		    the entry point and the place where main() returns to
		    after completion.

		    See file eg-06.c for further details.		    


 * eg-07.c	    Our real C-looking program.

   		    The example is similar to eg-06 but with some changes.

		    - We replaced the asm rt0 with a C version
		      (just'cause we can).
		    
		    - We defined the stack address in the linker script as a
		      macro and used the macro in rt0. This way we can control
		      the stack from the linker rather than having to modify
		      the program should we change our mind. It's just a
		      convenience.

		    - We used the linker script also to prepend rt0 to the
		      main program. This way we don't need to explicitly pass
		      the object as argument to the linker at the command line.
		      That gives the build procedure a look-and-feel more akin
		      to what we are conventionally used to.

		    - We renamed write_str() and call it puts(), the name of
		      the well-known function specified by the standard C
		      library. Moreover, we implemented puts fully compatible
		      with the ISO-C API, which determines that, on success

		      	   int puts (const char *p)

		      return the number of characters written.

		      Under the fastcall calling convention (as well as under
		      the default conventions employed by Linux and Windows),
		      the callee pass the return status to the caller using
		      the register %ax.

		    - We dismissed the attribute 'naked' with main(). This will
		      not prevent the compiler from adding a few extra
		      instructions in the assembly --- which, although not
		      strictly need, causes no harm either --- but also let us
		      use the regular 'return' keyword in main().

   		    See comments in the file.

  * eg-08.c 	    Finally our masterpiece: a Hello World that looks exactly
    		    like the legendary Kernighan & Ritchie's one.

    		    In this example we have just defined a printf macro that
		    actually calls puts. The function is not compatible with
		    the standard C's API (because printf does much more than
		    writing into the standard output), but it suffices to
		    reproduce original K&H's "Hello world".

		    Notice that, contrary to eg-07.h, we don't declare printf
		    in our own header. Instead, we just include the regular
		    stdio.h header provided by the standard C library (glibc).

		    Also, we don't need to pass the object containing the
		    puts() implementation (eg-08_utils.o). Instead, we
		    use the linker script that does it silently, not unlike
		    what happens when gcc quietly links hosted programs
		    against the standard libc.

		    You can see this in action by doing 

		       make eg-08.bin

		    to create all the files, then removing eg-08.bin with

		       rm eg-08.bin

		    and finally issuing the make command with the option

		       make ALT=1 eg-08.bin

		    The gcc command-line specifies only the options required
		    by a stand-alone bare-metal program, plus the linker
		    script. The runtime initializer (rt0) and our custom
		    "libc" are handled by the linker script.

		    Not bad, eh?


 Notes
 ------------------------------

 (1)   Original PC's BIOS used to read the MBR from the first 512 bytes of
       either the floppy or hard disks. Later on, when CD-ROM technology
       came about, it brought a different specification for the boot
       information, described in the iso9960 (the conventional CD file system
       format). Vendors then updated the BIOSes to detect whether the storage
       is either a floppy (or HD) or a CD-ROM, and apply the appropriate boot
       procedure. More recently, when USB flash devices were introduced, the
       approach adopted by BIOS vendors was not to specify a new boot procedure,
       but to emulate  some of the former devices. The problem is that this
       choice is not very consistent: some BIOSes would detect a USB stick as
       a floppy, whereas other BIOSes would see it as a CD (welcome to the
       system layer!).

       If your BIOS mimics the original PC, to make a bootable USB stick
       all you need to do is to copy the MBR code to its first 512 bytes:


   	  make stick IMG=foo.bin STICK=/dev/sdX


       On the other hand, if your BIOS insists in detecting your USB stick
       as a CD-ROM, you'll need to prepare a bootable iso9660 image as
       described in El-Torito specification [2]. 

       As for that, the GNU xorriso utility may come in handy: it prepares a
       bootable USP stick which should work in both scenarios. Xorriso copies
       the MBR code to the first 512 bytes to the USB stick and, in addition,
       it also transfer a prepared iso9660 to the right location. If you can't
       get your BIOS to boot and execute the example with the above (floppy)
       method, then try

         make stick IMG=foo.iso STICK=/dev/sdX


       We wont cover iso9660 El-Torito boot for CD-ROM, as newer x86-based
       computers now offers the Unified Extensible Firmware Interface meant
       to replace the BIOS interface of the original PC.  EFI is capable
       of decoding a format device, allowing the MBR code to be read from a
       standard file system. Although EFI is gradually turning original PC
       boot obsolete, however, most present day BIOSes offer a so called
       "legacy mode BIOS" and its useful to understand more sophisticated
       technologies, as low-level boot is still often found in legacy
       hardware and embedded systems.



 (2)  One reason we had better switch from nasm (Intel) to gas (AT&T)
      assembly here is because GCC (compiler) and NASM (assembler) do
      not go that well together. Actually, although GCC outputs AT&T syntax
      by default, it is true that GCC is capable of outputing Intel
      assembly if asked by means of the 'masm' option.  The problem is that
      the latter comes in specific Intel assembly dialect meant for GAS
      (GNU Assembler), which is not the same as that understood by NASM
      --- for instance, some directives (such as 'bit 16' vs .code16,
      times vs .fill etc.), data types ('dw' vs 'word'), and mnemonics
      (mov vs movb, movl etc.) differ.

      A glimpse of those differences may be seen in egx-01.S.

      It's therefore not practical to ask GCC to generate intel assembly
      from C, and have the latter assembled by nasm; we would need to
      use GAS, instead anyway.

      As a side note, that is why we do not refer to assembly as a
      "programming language", in the same way we refer to the C language
      but, as the name implies, as an "assembling language". Diverse
      assembly standards vary in syntax and thereby constitute different 
      specifications. Moreover, the assembly code is dependent on the
      processor architecture e.g. i386 vs ARM etc.

      If we want to read the assembly generated by GCC, even if we ask
      for Intel syntax, we would need to learn a new dialect --- whose
      differences are not only a matter of syntax, but also of semantics.
      For instance, the way NASM compiler works with directive 'org'
      is very different from how GAS works in association to the linker
      (ld) to relocate code.  It so happens that it may be arguably more
      interesting learn the native GCC toolchain assembly dialect and
      proceed therefrom.


 (3)  Previously, we would do
		    
      		    AT&T syntax			Intel syntax

      		    msg(%si)			[msg + si]


      to iterate through the characters of the string located at msg, using bx
      as the index of the array.

      Now, rather than constant labeled position, our string may be anywhere
      and we pass it to the function as an address stored in %cx. The thing
      is, we can't do

      Now, we don't have a label to reference the string. Instead, the
      write_str() function receives the parameter in register %cx (because
      the function attribute specify the fastcall calling convention).
      The thing is, we can't do this

		    AT&T syntax			Intel syntax

		    %cx(%bx)			[cx + bx]
		    
      Why not? Because the i8086 (16-bit precursos of x86) does not allow us to.
      The way to traverse an array here is by using the base-index-scale

		  base address + index array * scale

      E.g.

		   (%bx, %si, 1)

      is the memory position starting from the base address %bx, advancing
      %si * 1 positions. The scale factor is helpful if we want to consider
      %si the index of a vector whose elements spans multiple bytes. For
      instance, if %bx is the base of an integer vector, we can deference
      the 3rd element by doing

      	           mov 3, %si
      	      	   mov (%bx, %si, 4), %ax

      We can omit the scale factor if it's equal to 1.

      So, in our example, can we do

		  (%cx, %si)

       No.

       Real-mode x86 allow us limited choices

	     	   (%bx), (%bp), (%si), (%di),

	           (%bx,%si), (%bx,%di), (%bp,%si) and (%bp,%di)


        We had than to first copy %cx to %bx,
	
	(Do the respective names "base register" for %bx and "source  index 
	register" for si ring you a bell?)

	Do I really need to know all of this??? 

	"Hump... know this you must, for intricate and deceptive the hardware
	may be... and fighting the darkness you shall, to bring consistency
	and balance where there would be only chaos and fear."

 (4) Extend asm

     Program egx-03.c is similar to eg-05.c but uses GCC's extended assembly.

     Extended assembly augment the standard inline assembly functionality
     with several advanced features that may be quite useful. It's possible to

     	  - rerence the C-program's variables from within the inline asm code;
	  - automatically create labels that are unique across the entire
	    compilation (formally, translation) unit
	  - make GCC aware of which registers your inline asm has modified,
	    so as to avoid conflics with the remaing C code

      To exemplify, notice the line

          : b (s)

      in file egx-03.c. It tells the compiler that we want the value in local
      variable 's' (the function argument) to be stored in register %bx.
      (because later on we intend to use %bx to reference the string).
      To fulfill our requirement, the compiler adds some assembly code to
      copy %cx into %bx (remember, fastcall calling convention says that the
      first argument is passed by the caller to the callee in register %cx)

          mov %ecx, %edx
	  mov %edx, %bx

      We don't need to do that manually as in eg-05.c (standard asm).

      Incidentally, observe that the disassembled code shows the name of
      the 32-bit registers %ecx and %edx instead of the respective 16-bit 
      registers %cx and %dx. This is because the compiler decided to use the
      instruction 0x89 for the mov operation. Normally, instruction 0x89
      manipulate the 32-bit registers, but notice that the opcode is preceded by
      the value 0x66. That is called an operand-size prefix, and its effect
      is to modify the operand size of the following instruction (yep, there's
      this too). See note 5 for more on instruction prefix.  
      
      You may compare eg-05.bin (asm) and egx-03.bin (extended asm) with

         make a16 eg-05.bin egx-03.bin

      Because of the operand prefix, some corresponding lines are offset
      in the second program, as we can see in the disassembled code.
      
      Test eg-06.bin:

          make egx-03.bin/run

      Finally, notice that in extended asm GCC is allowed to optmize output. We used
      the volatile keyword with extended asm to prevent optimization. Even so,
      there may be changes made by the compiler. If you want to be sure that your
      inline assembly goes untouched to the compiler output, do not use extended
      asm. Rather, stick to the standard asm.


 (5) Instruction prefix

     Some x86 instructions may be prefixed with certain bytes. If present, the
     prefix modifies the original behavior of the instruction.

     For instance, 0x66 before a mov instruction changes the size of the
     operand: if the instruction normally manipulate a 32-bit value, the 0x66
     prefix, named operand-size override, causes the instruction to operate with
     a 16-bit operand. The reverse is also true: if the mov instruction would
     operate with 16-bit registers, it now operates with 32-bit. There are
     several kind of prefixes which change operand and addresses sizes, override
     segment registers and perform other modifications.

     If you disassemble eg-06.bin with

        make egx-03.bin/a16

     you'll see two mov instructions prefixed with 0x66. Moreover, the assembly
     output will read

     	mov %ecx, %edx
	mov %edx, %ebx

     This is because the command is telling the disassemble to assume a 16-bit
     and in such an architecture, the operand prefix has no meaning (the
     instruction still works because the real-mode in a modern x86 hardware
     is actually an emulation, and 32-bit instructions still work. For some
     reason only GCC-gurus will reveal to you, the compiler opted to use a
     0x66-prefixed 32-bit mov.

     If you otherwise disassemble the program with

       make egx-03.bin/a32

     instead, you are asking the disassemble to assume that the cpu is 32-bit,
     and the disassembler will honor the prefix, showing

       mov %cx, %dx
       mov %dx, %bx

     Now, the disassemble knows that the instruction will operate on %cx and %dx.
 
DOCM4_BINTOOLS_DOC


 References
 ------------------------------
 
 [1] Auxiliary program: syseg/src/hex2bin

 [2] El-Torito: https://wiki.osdev.org/El-Torito




