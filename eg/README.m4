include(docm4.m4)

 EXAMPLES
 ==============================

DOCM4_DIR_NOTICE

 CONTENTS
 ------------------------------

 Contents of syseg/eg

 Please refer to README in each subdirector for detailed information.

 * hw 	      Hello World bare metal (from machine code, to asm, up to C)
 * real	      Real-mode: segmented memory, memory-mapped io etc.
 * build      Basics of building a C program, static library
 * make	      Build automation with make, the principles
 * abi	      Application-binary interface


