/* syscall.h - C header file.
 
   Copyright (c) 2020-2022 - Monaco F. J. <monaco@usp.br>

   This file is part of SYSeg. 

   SYSeg is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   SYSeg repository is accessible at https://gitlab.com/monaco/syseg

*/

#ifndef SYSCALL_H
#define SYSCALL_H

#define SYS_WRITE 0

/* void  syscall_handler(void); */

void __attribute__((naked)) sys_write(void);

void  __attribute__((naked)) syscall_handler (int, int, int, int);

#endif	/* SYSCALL_H */
