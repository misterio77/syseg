/* syscall.c - C source file.
 
   Copyright (c) 2020-2022 - Monaco F. J. <monaco@usp.br>

   This file is part of SYSeg. 

   SYSeg is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   SYSeg repository is accessible at https://gitlab.com/monaco/syseg

*/


#define pop_all_but_ax\
     "     pop %bx                  ;"\
     "     pop %bx                  ;"\
     "     pop %cx                  ;"\
     "     pop %dx                  ;"\
     "     pop %si                  ;"\
     "     pop %di                  ;"\
     "     pop %bp                  ;"


/* Write a string on the standard output.
   Arguments:  bx   pointer to the string. */

void __attribute__((naked)) sys_write(void)
{				
  __asm__
    (
     "     pusha                    ;" /* Push all registers.               */

     "     mov   $0x0e, %ah         ;" /* Video BIOS service: teletype mode */
     "     mov   $0x0, %si          ;"
     "loop2:                        ;"
     "     mov   (%bx, %si), %al    ;" /* Read character at bx+si position. */
     "     cmp   $0x0, %al          ;" /* Repeat until end of string (0x0). */
     "     je    end2               ;"
     "     int   $0x10              ;" /* Call video BIOS service.          */
     "     add   $0x1, %si          ;" /* Advace to the next character.     */
     "     jmp   loop2              ;" /* Repeat the procedure.             */
     "end2:                         ;"

     "     mov %si, %ax             ;" /* Return string length in %ax.      */
   
     pop_all_but_ax
     
     "     ret                      ;"
     );
}


/* Array pointing to all syscall functions. */

  
void (*syscall_table[])() =
  {
    sys_write			/*   0 : SYS_WRITE. */
  };


/* Syscall handler. 

   Attribute regparm(2) modifies the calling convention such that
   the arguments are passed through registers ax, dx and cx, respectively.
   Syscall functions should look for their arguments in those registers.

*/

void  __attribute__((naked))
syscall_handler (int number, int arg1, int arg2, int arg3)
{
  
  syscall_table[number]();

  __asm__("iret");
  
}



/* Notes.



 */
