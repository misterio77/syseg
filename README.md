# SYSeg - System Software by Example

SYSeg (System Software by example) is a collection of code examples and programming
exercises intended for illustration of general concepts and techniques concerning
system software design and implementation. It may be useful as a companion 
resource for students and instructors exploring this field.

Copyright (c) 2019-2023 Monaco F.J. - Disributed under GNU GPL vr. 3

(see enclosed file COPPYING for further information)

## Quick start

- Some code examples and programming exercises require auxiliary artifacts 
(including documentation) which must be built beforehand. 

  In order to get thos items built you must perform the setup procedure.

  Please, read `syseg/doc/manual.md' and proceed as explained.

  There you will also find instructions on how to use SYSeg code examples.
 

- For copyright and licensing information, please refer to the file 'COPYING' at
  the root of the project's source tree.

- Please, sync your local repository regularly as SYSeg is being updated
  regularly (`git pull && ./configure && make`).

## Overview

SYSeg contents. 

- Directory `eg` contains source code examples.
- Directory `try` contains programming exercises.
- Directory `tools` contains auxiliary tools used by examples and exercises.
- Directory `doc` contains SYSeg documentation.
- Directory `extra` contains additional material about advanced topics, non-standard features, side notes, and hacker lore.

## Contributing

But reports and suggestions are always welcome.

Please, contact the author.