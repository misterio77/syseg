 USEFUL REFERENCES
 ==============================

 This is a non-exhaustive list of online references with information of  
 interest for system software developers, including manuals, tutorials,   
 specifications and other useful resources.

 Manuals
 ------------------------------

 GNU Software

 libc (c library)	https://www.gnu.org/software/libc/documentation.html
 cpp (preprocessor)	https://gcc.gnu.org/onlinedocs/cpp/
 gcc (compiler)		https://gcc.gnu.org/onlinedocs/gcc
 as (assembler)		https://sourceware.org/binutils/docs-2.40/as/index.html
 ld (linker) 		https://gcc.gnu.org/onlinedocs/ld
 make (build autom.)	https://www.gnu.org/software/make/manual/
 binutils		https://sourceware.org/binutils/docs-2.40/binutils/index.html

 ar, objdump, readelf, nm: see binutils


 Tutorials
 ------------------------------


 Specifications

 ------------------------------

 - Intel® 64 and IA-32 Architectures,
   Software Developer’s Manual
   Volume 3A:
   System Programming Guide, Part 1

   https://www.intel.com/content/dam/www/public/us/en/documents/manuals/64-ia-32-architectures-software-developer-vol-3a-part-1-manual.pdf

 - TECH Help
   Lots of technical details of BIOS and DOS, by Crone and Rollings (Flambeux Software Inc.)

   http://www.techhelpmanual.com/


 Demonstrations
 ------------------------------

 Personal blogs and websites
 ------------------------------

 - PC real-mode 

 The Starman's ream https://thestarman.pcministry.com/


 Lore
 ------------------------------
