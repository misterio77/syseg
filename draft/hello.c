/* The hello.S program translated into C with inline asm. */


	
void _start(){
  __asm__
    (
"	cli;"
"	xor %ax, %ax;"
"	mov %ax, %ds;"
"	mov %ax, %es;"
"	mov %ax, %fs;"
"	mov %ax, %gs;"
"	mov %ax, %ss;"
"	mov $0xbff, %sp;"
"	jmp $0x0,$init0;"
"init0:	;"
"	sti;"
"	call main;"
"halt:;"
"	hlt;"
"	jmp halt;"
     );
}

int main(){
  __asm__
    (
"	mov msg, %cx"
"	call puts"
"	mov $0x0, %ax"
"	ret"
     );	  
}

int puts(){
  __asm__
    (
"	mov $0x0, %si;"
"	mov $0xe, %ah;"

"loop:;"
"	mov msg(%si), %al;"
"	cmp $0x0, %al;"
"	je end;"
"	int $0x10;"
"	inc %si;"
"	jmp loop;"
"end:;"
"	mov %si, %ax;"
"	ret;"

"msg:;"
"	.string \"Hello World\";"
	
"	. = 510 - _start;"

"	.word 0xaa55"
     );
}
